# Swarm
> mega millions irc bot swarm, fork of acidvegas' Jupiter

## Commands
| Command                 | Description                                                                                               |
| ----------------------- | --------------------------------------------------------------------------------------------------------- |
| 5000 \<chan>            | Emulates SuperNETs #5000 channel *(Joins \<chan> and will PM bomb anyone who joins the channel)*          |
| id                      | Send bot identity                                                                                         |
| raw [-d] \<data>        | Send \<data> to server, optionally delayed with -d argument                                               |
| relay \<+/->\<chan>     | Add (+) or Remove (-) \<chan> from relay list *(relays all \<chan> messages to hub channel)*              |
| relay -all              | Stops all relays                                                                                          |
| monitor list            | Return MONITOR list                                                                                       |
| monitor reset           | Reset MONITOR list                                                                                        |
| monitor \<+/->\<nicks>  | Add (+) or Remove (-) \<nicks> from MONITOR list. *(Can be a single nick or comma seperated list)*        |
| cover \<+/->\<nicks>    | Add (+) or Remove (-) \<nicks> from kick protection *(Bot will kick anyone who kicks \<nick>)*            |
| admin \<+/->\<nicks>    | Add (+) or Remove (-) \<nicks> from admin privileges                                                      |
| auto-op \<on/off>       | Toggles auto +o on join for admins                                                                        |
| migrate \<chan>         | Migrates the bots' central hub chan                                                                       |

**Note:** All commands must be prefixed with @all or the bots nick & will work in a channel or private message.

Raw data must be IRC RFC compliant data & any nicks in the MONITOR list will be juped as soon as they become available.

